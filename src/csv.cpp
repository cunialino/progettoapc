#include "csv.h"
#include<iostream>
#include<fstream>
#include<sstream>
#include <vector>

void csv::parse_csv(const std::string &filename, std::size_t n_coord, std::size_t pos_index, bool header, const char & sep){
  pos.clear();
  col_names.clear();
  fields.clear();
  std::ifstream istr {filename};
  if (!istr)
  {
    std::cerr << "Can’t open input file" << filename << std::endl;
    return;
  }
  rows = 0;
  cols = 0;
  std::string line;
  bool first_line = true;
  while (std::getline(istr, line)){
      rows++;
      std::istringstream ss(line);
      std::string ll;
      while(first_line && std::getline(ss, ll, sep)){
        cols++;
      }
      first_line = false;
    }
  istr.clear();
  istr.seekg(0);
  if(header)
  {
    has_header = true;
    --rows;
    std::string head_line;
    std::getline(istr,head_line);
    std::istringstream line_to_read(head_line);
    std::string name;
    while(std::getline(line_to_read, name, sep)){
  //    std::cout << name << " ";
      col_names.push_back(name);
    }
  }
  pos_index--;

  cols-=n_coord;
  std::string new_line;
  std::string obj;
  double obj_to_insert;
  std::vector<double> new_pos;
  new_pos.reserve(n_coord);
  fields.resize(rows*cols);
  std::size_t i=0;
  while(std::getline(istr, new_line))
  {
    std::istringstream line_to_read(new_line);
    std::size_t pp = 0;
    std::size_t j = 0;
    while(std::getline(line_to_read, obj, ','))
    {
      obj_to_insert = std::stod(obj);
      //now I have to distinguish between data and coordinates
      if(j >= pos_index && pp < n_coord ){
        new_pos.push_back(obj_to_insert);
        pp++;
      }
      else{
        fields[i+j*rows]=obj_to_insert;
        j++;
      }
    }
    point new_point(new_pos);
    new_pos.clear();
    pos.push_back(new_point);
    i++;
  }
  return;
}
void csv::write_csv(const std::string & filename, std::vector<double> const & vals, std::size_t c, const char & sep){

  std::ofstream out(filename);
  if(!out){
    std::cerr << "File non esistente" << std::endl;
    return;
  }
  for(std::size_t i = 0; i < vals.size(); i++){
    if((i+1)%c == 0){
      out << vals[i] << "\n";
    }
    else{
      out << vals[i] << sep;
    }
  }
  out.close();
  return;
}
