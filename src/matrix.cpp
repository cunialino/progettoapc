#include "matrix.h"
#include <cassert>
#include <algorithm>
#include <ctime>
#include <cmath>


#if defined (USE_LAPACK)

#define dgemm dgemm_
#define DGEMM dgemm_

extern "C"
{
  void
  dgemm (const char *TRANSA, const char *TRANSB, const int *M,
         const int *N, const int *K, const double *ALPHA,
         const double *A, const int *LDA, const double *B,
         const int *LDB, const double *BETA, double *C,
         const int *LDC);
}

#define dposv dposv_
#define DPOSV dposv_
extern "C"{
  void dposv	(const char *UPLO, const int *N, const int* NRHS, const double *A, const int *LDA, double *B, const int *LDB, int *INFO);

}

#define dpotrs dpotrs_
#define DPOTRS dpotrs_
extern "C"{
  void dpotrs(const char* UPLP, const int *N, const int* NRHS, const double *A, const int *LDA, double* B, const int *LDB, int *info);
}
#endif

matrix matrix::transpose () const{

  matrix retval (get_cols (), get_rows ());
  unsigned int i, j;
  for (j = 0; j < retval.get_cols (); ++j)
    for (i = 0; i < retval.get_rows (); ++i)
      retval (i, j) = const_index (j, i);
  return (retval);
}

#if defined (USE_LAPACK)
matrix operator* (const matrix& A, const matrix& B){

  int M = static_cast<int>(A.get_rows ());
  int N = static_cast<int>(B.get_cols ());
  int K = static_cast<int>(A.get_cols ());

  char ntr = 'n';
  double one = 1.0;
  double zero = 0.0;

  matrix retval (A.get_rows(), B.get_cols());

  dgemm (&ntr, &ntr, &M, &N, &K,
         &one, A.get_data (), &M,
         B.get_data (), &K, &zero,
         retval.get_data (), &M);

  return (retval);
}

matrix matrix::solve(matrix &rhs){
  matrix tmp = rhs;
  char const uplo='L';
  int N=static_cast<int>(rows);
  int NRHS = static_cast<int>(rhs.get_cols());
  int resp;
  if(factorized)
    dpotrs(&uplo, &N, &NRHS, get_data(), &N, tmp.get_data(), &N, &resp);
  else
    dposv(&uplo, &N, &NRHS, get_data(), &N, tmp.get_data(), &N, &resp);
  for(std::size_t i =0; i < rows; i++)
    for(std::size_t j=i+1; j < cols; j++)
      index(i, j)=0;
  factorized = true;
  return tmp;
}

matrix matrix::inv_chol(void){
  matrix id(get_rows());
  for(std::size_t i=0; i < rows; i++)
    id(i, i)=1;
  return solve(id);
}
#else
matrix operator* (const matrix& A, const matrix& B){
  unsigned int i, j, k;
  assert (A.get_cols () == B.get_rows ());
  matrix retval (A.get_rows (), B.get_cols ());
  matrix tmp = A.transpose ();
  for (i = 0; i < retval.get_rows (); ++i)
    for (j = 0; j < retval.get_cols (); ++j)
      for (k = 0; k < A.get_cols (); ++k)
        retval(i,j) += tmp(k,i) * B(k,j);
  return (retval);
}

matrix matrix::solve (matrix &rhs){
  std::size_t ii, jj, kk;
  double f;
  matrix retval = rhs;

  // Factorize
  if (! factorized)
    { factorize (); /*std::cout << "factorize !" << std::endl;*/ }

  // Do Forward Substitution
  for (ii = 0; ii < get_rows (); ++ii){
      f = retval(ii, 0);
      for (kk = 0; kk < ii; ++kk)
        f -= index (ii, kk) * retval(kk, 0);
      retval(ii, 0) = f/index(ii, ii);
    }

  // Do Backward Substitution
  for (jj = 1; jj <= get_rows (); ++jj)
    {
      ii = get_rows () - jj;
      f = retval(ii, 0);
      for (kk = ii+1; kk < get_cols (); ++kk)
          f -= index (kk, ii) * retval( kk, 0 );
      retval( ii, 0 ) = f / index (ii, ii);
    }
  return retval;
}
matrix matrix::inv_chol(void){
  matrix X(get_rows());
  for(unsigned i=0; i < X.get_cols(); i++){
    matrix xi(X.get_rows(), 1);
    matrix rhs(X.get_rows(), 1);
    rhs(i, 0)=1.0;
    xi = solve(rhs);
    for(unsigned j=0; j < X.get_rows(); j++){
      X(j, i) = xi(j, 0);
    }
  }
  return X;
}

#endif



void matrix::factorize () {
  std::size_t N = get_rows();
  matrix tmp(N);
  for(unsigned i =0; i < N; i++){

    for(unsigned j = 0; j <= i; j++){

      double sum = 0;
      for(unsigned k=0; k < j; k++){

        sum += tmp(i, k)*tmp(j, k); //A(i, k)*A(j, k);

      }
      tmp(i, j) = index(i, j)-sum;
      if(i == j){
        tmp(i, j)=sqrt(tmp(j, i));
      }
      else{
        tmp(i, j) = tmp(i, j)/tmp(j, j);
      }
    }
  }
  data.swap(tmp.data);
  factorized = true;
};
matrix& matrix::operator=(const matrix& B){
  rows=B.rows;
  cols=B.cols;
  factorized=B.factorized;
  data = B.data;
  return *this;
}

matrix matrix::operator-(const matrix& B){
  matrix retval(get_rows(), get_cols());
  for(unsigned i=0; i < get_rows(); i ++){
    for(unsigned j=0; j < get_cols(); j++){
      retval(i, j)=index(i, j)-B(i, j);
    }
  }
  return retval;
}
matrix& matrix::operator*=(double d){
  for(unsigned i = 0; i < cols; i++){
    for(unsigned j = 0; j < rows; j++){
      index(i, j) = d*index(i, j);
    }
  }
  return *this;
}

void matrix::swap_data(std::vector<double> vals, unsigned r, unsigned c){
  std::swap(vals, data);
  rows=r;
  cols=c;
  return;
}
std::ostream & operator<<(std::ostream & os, const matrix & A){
  for(std::size_t i = 0; i < A.get_rows(); i++){
    for(std::size_t j = 0; j < A.get_cols(); j++){
      os << A(i, j) << " ";
    }
    os << std::endl;
  }
  return os;
}
