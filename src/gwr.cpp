#include <boost/math/distributions/students_t.hpp>
#include <cmath>
#include <random>
#include <algorithm>
#include <set>
#include "gwr.h"
#include "NumericalAlgorithms.h"
#include <vector>
#include <iostream>
#include <limits>
#if defined (MONTECARLO_PARALLEL)
  #include <mpi.h>
#endif

namespace lm{

  void gwr::max_bandwidth_initialization() {
    std::set<double, std::greater<double>> dist;
    for(auto it1 = datas_pos.cbegin(); it1 < datas_pos.cend(); it1++){
      for(auto it2 = it1+1; it2 < datas_pos.cend(); it2++){
        double d =(*it1).distance(*it2);
        dist.insert(d);
      }
    }
    bandwidth_range = std::make_pair(*(++dist.rbegin()), *dist.begin());
    std::cout << "Range dist: " << bandwidth_range.first << " " << bandwidth_range.second << std::endl;
  }

  matrix bisquare_ker(point ref_pos, std::vector<point> const & pos, double h, std::size_t num_i, bool no_i){
    matrix retval(pos.size());
    for(unsigned i=0; i < pos.size(); i ++){
      double d=ref_pos.distance(pos[i]);
      if(d>h || (no_i && i == num_i))
        retval(i, i) = 0;
      else{
        d*=d;
        retval(i, i) = std::pow(1-d/(h*h), 2);
      }
    }
    return retval;
  }


  matrix gaussian_ker(point ref_pos, std::vector<point> const & pos, double h, std::size_t num_i, bool no_i){
    matrix retval(pos.size());
    for(unsigned i=0; i < pos.size(); i ++){
      if(no_i && i == num_i)
        retval(i, i) = 0;
      else
      {
        double d=ref_pos.distance(pos[i]);
        d*=d;
        retval(i, i) = std::exp(-d/(2*h*h));
      }
  }
  return retval;
}
  void gwr::fit(bool no_i){
    beta_hats.clear();
    y_hats.clear();
    errs.clear();
    if(bandwidth_optimized){
      bandwidth = opt_bandwidth;
    }
    for(std::size_t i = 0; i < reg_pos.size(); i ++){
      matrix weights;
      weights = ker(reg_pos[i], datas_pos, bandwidth, i, no_i);
      matrix min_sqr = X.transpose()*weights*X;
      matrix rhs = X.transpose()*weights*y;
      fit_minsqr(min_sqr, rhs);
      beta_hats.push_back(beta_hat);
      y_hats.push_back(y_hat);
      if(no_i){
        err(i, 0) = 0;
      }
      errs.push_back(err);
    }
    return;
  }

  double gwr::cvss(double h){
    double retval = 0;
    bandwidth = h;
    fit(true);
    for(std::size_t i = 0; i < errs.size(); i++){
      matrix tmp = errs[i].transpose()*errs[i];
      retval += tmp(0, 0);
    }
    //std::cout << "cvss(" << h << ")=" << retval << std::endl;
    return retval;
  }

  double gwr::bandwidth_selection(void){
    //opt_bandwidth = my_numerics::gradient_descent_momentum(std::bind(&gwr::cvss, this, std::placeholders::_1), bandwidth_range.second);
    opt_bandwidth = my_numerics::golden_selection(std::bind(&gwr::cvss, this, std::placeholders::_1), bandwidth_range.first, bandwidth_range.second);
    bandwidth_optimized = true;
    return opt_bandwidth;
  }

  double gwr::compute_v_stat(std::size_t j){
    double v = 0;
    double mean = 0;
    fit();
    for(std::size_t i = 0; i < beta_hats.size(); i++){
      mean += beta_hats[i](j, 0);
    }
    mean = mean/static_cast<double>(beta_hats.size());
    for(std::size_t i = 0; i < beta_hats.size(); i ++){
      v+= std::pow(beta_hats[i](j, 0)-mean, 2);
    }
    v = v/static_cast<double>(beta_hats.size());
    return v;
  }

#if defined (MONTECARLO_PARALLEL)
  std::vector<double> gwr::monte_carlo_test(const std::size_t NUMSHUFFLE){

    int r(0), s(0);
    MPI_Comm_rank(MPI_COMM_WORLD, &r);
    MPI_Comm_size(MPI_COMM_WORLD, &s);

    const std::vector<point> RIGHT_POS = datas_pos;
    bool was_optimized = bandwidth_optimized;
    const double TMP_OPT = opt_bandwidth;
    std::vector<double> retval(beta_hat.get_rows(), 0);

    std::random_device rd;
    std::mt19937 g(rd());
    //Cycle on all coefficients
    for(std::size_t i = 0; i < retval.size(); i++){
      //building the statistics
      //Saving value for correct positions
      double right_pos_v = compute_v_stat(i);
      unsigned R = 0;
      std::shuffle(datas_pos.begin(), datas_pos.end(), g);
      for(std::size_t j = 0; j < NUMSHUFFLE; j+=static_cast<std::size_t>(s)){
        //Shuffling randmly the positions
        std::next_permutation(datas_pos.begin(), datas_pos.end());
        //inserting the new statistic
        fit();
        double stat = compute_v_stat(i);
        if(stat >= right_pos_v)
          R++;
      }
      MPI_Allreduce(MPI_IN_PLACE, &R, 1, MPI_UNSIGNED, MPI_SUM, MPI_COMM_WORLD);
      retval[i] = static_cast<double>(R)/static_cast<double>(NUMSHUFFLE);
    }
    datas_pos = RIGHT_POS;
    if(was_optimized){
      opt_bandwidth=TMP_OPT;
      bandwidth_optimized = true;
    }
    fit();
    return retval;
  }


#else
  std::vector<double> gwr::monte_carlo_test(const std::size_t NUMSHUFFLE) {
    //Save variables for true position
    //std::cout << "Starting montecarlo simulation" << std::endl;
    const std::vector<point> RIGHT_POS = datas_pos;
    //Initializing vector I'll return (true if Bi!=0)
    std::vector<double> retval(beta_hat.get_rows(), 0);
    std::random_device rd;
    std::mt19937 g(rd());
    //Cycle on all coefficients
    for(std::size_t i = 0; i < retval.size(); i++){
      //building the statistics
      //Saving value for correct positions
      double right_pos_v = compute_v_stat(i);
      std::size_t R =0;
      std::shuffle(datas_pos.begin(), datas_pos.end(), g);
      for(std::size_t j = 0; j < NUMSHUFFLE-1; j++){
        //Shuffling randmly the positions
        std::next_permutation(datas_pos.begin(), datas_pos.end());
        fit();
        double stat = compute_v_stat(i);
        if(stat >= right_pos_v)
          R++;
      }
      retval[i] = static_cast<double>(R)/static_cast<double>(NUMSHUFFLE);
    }
    datas_pos = RIGHT_POS;
    fit();
    return retval;
  }
#endif

}
