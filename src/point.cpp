#include "point.h"
#include <cmath>


double point::distance(point const & p2) const{
  double sum = 0;
  for(unsigned i = 0; i < dim; i++ ){
    sum+= (coords[i]-p2.coords[i])*(coords[i]-p2.coords[i]);
  }
  return std::sqrt(sum);
}
bool point::operator<(point const &p2){
  bool retval = true;
  for(std::size_t i = 0;retval && i < dim; i++){
    if(coords[i] > p2.coords[i]){
        retval = false;
    }
  }
  return retval;
}
