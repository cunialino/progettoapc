#include "min_square.h"
#include <iostream>

namespace lm{
  void min_square::fit_minsqr(matrix & min_sqr, matrix & rhs){
    beta_hat = min_sqr.solve(rhs);
    y_hat = X*beta_hat;
    err = y - y_hat;
    double err_sqr = 0;
    std::size_t N = err.get_rows();
    err_sqr = (err.transpose()*err)(0, 0);
    sigma_hat = err_sqr/static_cast<double>(N-X.get_cols());
    return;
  }
}
