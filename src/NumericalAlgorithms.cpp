#include <functional>
#include <iostream>
#include "NumericalAlgorithms.h"
#include <cmath>

namespace my_numerics{
  double gradient_descent(std::function<double (double)>f, double h0, double step, double thres){
    /*std::cout << "Step for finite diff is: " << step << std::endl;
    std::cout << "Starting point is: " << h0 << std::endl;
    std::cout << "Toll is: " << thres << std::endl;*/
    constexpr unsigned MAX_ITER = 1000;
    constexpr double alpha = 0.8;
    double x=h0; 
    bool converged = false;
    for(unsigned it = 0; not converged && it < MAX_ITER ; it++){
      double x1 = x+step;
      if(x1 == x){
        std::cerr << "Problema" << std::endl;
      }
      double diff = (f(x+step)-f(x-step))/(2*step);
      x -= alpha*diff;
      converged = (std::abs(diff) < thres)?true:false;
      if(converged){
        std::cout << std::abs(diff) << std::endl;
      }
    }
    if(not converged){
      std::cerr << "Method did non converge" << std::endl;
    }
    return x;
  }

  double gradient_descent_momentum(std::function<double (double)>f, double h0, double step, double thres){
    constexpr unsigned MAX_ITER = 10000;
/*    std::cout << "Starting Gradient descent with momentum:" << std::endl;
    std::cout << "\tStep for finite diff is: " << step << std::endl;
    std::cout << "\tStarting point is: " << h0 << std::endl;
    std::cout << "\tToll is: " << thres << std::endl;
    std::cout << "\tMac iter: "<< MAX_ITER << std::endl;*/
    constexpr double alpha = 0.001;
    constexpr double beta = 0.9;
    double x=h0; 
    bool converged = false;
    double m = 0;
    unsigned it =0;
    for(; not converged && it < MAX_ITER ; it++){
      double x1 = x+step;
      if(x1 == x){
        std::cerr << "Problema" << std::endl;
        step = 0.00001*x;
        std::cout << "New step: " << step << std::endl;
      }
      double diff = (f(x+step)-f(x-step));
      diff = diff/(2*step);
      m=beta*m+alpha*diff;
      x -= m;
      converged = (std::abs(diff) < thres)?true:false;
    }
    if(not converged){
      std::cerr << "Method did non converge" << std::endl;
    }
    else{
      //std::clog << "Method converged in " << it << " iterations" << std::endl;
    }
    return x;
    }
    
  double golden_selection(std::function<double (double)> f, double a, double b, double toll){
    double gr = (1+std::sqrt(5))/2;
    double c=b-(b-a)/gr;
    double d=a+(b-a)/gr;
    constexpr unsigned MAX_ITER = 100;
    for(unsigned i = 0; std::abs(c-d) > toll && i < MAX_ITER; i++){
      if(f(c) < f(d))
        b=d;
      else
        a=c;
      c=b-(b-a)/gr;
      d=a+(b-a)/gr;
    }
    return (b+a)/2;
  }

}
