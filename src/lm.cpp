#include "lm.h"
#include "matrix.h"
#include <cmath>
#include <boost/math/distributions/students_t.hpp>
#include <iostream>

namespace lm{
  void print2(matrix const &A){
    for(unsigned i = 0; i < A.get_rows(); i++){
      for(unsigned j = 0; j < A.get_cols(); j++){
        std::cout << A(i, j) << " ";
      }
      std::cout << std::endl;
    }
  }
  void linear_model::t_tests(matrix & min_sqr){
    matrix cov_b_hat(min_sqr.get_cols());
    cov_b_hat = min_sqr.inv_chol();
    cov_b_hat*=sigma_hat;
    std::size_t dof = err.get_rows()-X.get_cols();
    boost::math::students_t dist(static_cast<double>(dof));
    for(unsigned i = 0; i < beta_hat.get_rows(); i++){
      double t_stat = beta_hat(i, 0)/std::sqrt(cov_b_hat(i, i));
      p_val[i] = 2*(1-cdf(dist, std::abs(t_stat)));
    }
    return;
  }

  void linear_model::fit(){
    matrix min_sqr = X.transpose()*X;
    matrix rhs = X.transpose()*y;
//    print(min_sqr);
    fit_minsqr(min_sqr, rhs);
    t_tests(min_sqr);
    return;
  }

}
