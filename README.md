# Geographically weighted regression
## Implemented by
* Elia Cunial (elia.cunial@mail.polimi.it)
* Carlo Augusto Vitellio (carloaugusto.vitellio@mail.polimi.it)

(source code avaible on [GitLab](https://gitlab.com/cunialino/progettoapc/))

## Description
* **src:** contains all the source files for the each classes
* **include:** contains all the header files for each class
* **test:** here there are several directories with all the tests we made, in each one of them there is a make file to compile the code. 

## Requirements
* C++ compiler (g++)
* MPI (tested with openmpi v4.0.1)
* Boost library (tested with v1.69)
* lapack library (tested with v3.8.0)
* blas library (tested with v3.8.0)

## Makefile usage
There are three intened usages:
* `make all` will compile the code with no parallelization and no usage of lapack libraries
* `make lapack` will compile the code with no parallelization but using lapack
* `make parallel` will compile the code with both parallelization (only montecarlo test is implemented in parallel) and lapack.

## Crucial Tests explanation
* **LapVsNoL:** in this directory one can find the speed comparison between the implementation with and without lapack, we generated random definitive positive matrices in order to fit a gwr model with an N increasing. After you run the code (one can just run the bash script "comparison.sh") you can plot the results with the R-script
* **NY8:** here you can find the speed comparison between parallel and non parallel implementetion of the montecarlo test, again after running the code (you can just run the bash script "comparison.sh")
* **London:** here we tried to see if the montecarlo test is actually working, if you run the code you'll see the p-values of the [dataset](https://rdrr.io/cran/GWmodel/man/LondonHP.html) for the model PURCHASE~PROF+FLOORSZ
