#include "matrix.h"
#include "lm.h"
#include <iostream>
#include <cmath>

using namespace lm;

int main(){
  unsigned N=25;
  matrix X(N, 2), y(N, 1);
  for(unsigned i=0; i < N; i++){
    X(i, 0)=1;
    std::cin >> X(i, 1);
    std::cin >> y(i, 0);
  }
  linear_model ll(X, y);
  ll.fit();
  matrix beta = ll.get_beta();
  std::cout << "Beta_hat: " << std::endl;
  std::cout << beta << std::endl;
  std::cout << "Sigma hat: " << std::sqrt(ll.get_sigma_hat()) << std::endl;
  std::vector<double> pval = ll.get_pvalues();
  std::cout << "PVAL" << std::endl;
  for(auto i : pval)
    std::cout << i << std::endl;
  std::cout << "DONE" << std::endl;
  return 0;
}
