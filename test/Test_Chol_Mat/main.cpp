#include "matrix.h"
#include <iostream>

void print(matrix const A){
  for(unsigned i = 0; i < A.get_rows(); i++){
    for(unsigned j = 0; j < A.get_cols(); j++){
      std::cout << A(i, j) << " ";
    }
    std::cout << std::endl;
  }
}

int main(){
  constexpr unsigned N = 3;
  matrix A(N);
  for(unsigned i = 0; i < N; i++){
    for(unsigned j = 0; j<N; j++){
      std::cin >> A(i, j);
    }
  }
  print(A);
/*  A.factorize();
  print(A);
  print(A*A.transpose());*/
  matrix b(N, 1);
  for(unsigned i = 0; i< N; i++)
    b(i, 0)=1;
  //print(b);
  matrix sol = A.solve(b);
  print(sol);
  print(A);
  std::vector<double> identity(N*N);
  for(std::size_t i=0; i < identity.size(); i+=N+1)
    identity[i] = 1;
  matrix I(identity, N, N);
  //print(sol);
  //print(A-A);
  matrix X(N);
  X=A.inv_chol();
  std::cout << "INVERSE:" << std::endl;
  print(X);
  std::cout << "A:" << std::endl;
  print(A);
  matrix OLD(N);
  sol = A.solve(b);
  print(sol);
  return 0;
}
