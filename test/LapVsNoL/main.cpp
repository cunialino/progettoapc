#include "gwr.h"
#include "matrix.h"
#include "point.h"
#include "csv.h"

#include <vector>
#include <fstream>
#include <iostream>
#include <random>
#include <cmath>
#include <ctime>
#include <string>

matrix generate_random_matrix(std::size_t N, std::size_t M){
  matrix A(N, M);
  std::random_device rd;
  std::mt19937 e(rd());
  std::uniform_real_distribution<> uniform_dist(-1000, 1000);
  for(std::size_t i = 0; i < N; i ++){
    for(std::size_t j = 0; j < M; j++){
      A(i, j) = uniform_dist(e);
    }
  }
  return A;
}
using namespace lm;

int main(){
  #if defined (USE_LAPACK)
    std::string filename("output_lapa.csv");
  #else
    std::string filename("output_nol.csv");
  #endif

  csv csv_man;
  std::random_device rd;
  std::mt19937 e(rd());
  std::uniform_real_distribution<> uniform_dist(-1000, 1000);
  std::size_t num = 5;
  std::vector<double> times(num);
  std::vector<double> sizes(num);

  std::size_t N = 32, M=5;
  for(std::size_t i =0; i < num; i++, N*=2){
    matrix X = generate_random_matrix(N, M);
    matrix y = generate_random_matrix(N, 1);
    std::vector<point> pos(N);
    for(std::size_t k = 0; k < N; k++){
      point tmp(2);
      tmp.set_ith_coord(1, uniform_dist(e));
      tmp.set_ith_coord(2, uniform_dist(e));
      pos[k] = tmp;
    }
    gwr lm(y, X, pos, pos);
    std::cout << "fitting with N=" << N << std::endl;
    clock_t cs, ce;
    lm.bandwidth_selection();
    cs = clock();
    lm.fit();
    ce = clock() - cs;
    times[i] = static_cast<double>(ce)/CLOCKS_PER_SEC;
    sizes[i] = static_cast<double>(N);
  }
  std::vector<double> oo;
  oo.insert(oo.end(), sizes.begin(), sizes.end());
  oo.insert(oo.end(), times.begin(), times.end());
  csv_man.write_csv(filename, oo, sizes.size());

  return 0;
}
