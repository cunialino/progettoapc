dati <- read.csv("output_lapa.csv", sep = ";", header = FALSE)
lapa <- as.numeric(dati[2, ])
N <- as.numeric(dati[1, ])
dati <- read.csv("output_nol.csv", sep=";", header = FALSE)
nola <- as.numeric(dati[2, ])
pow = 3
ymax = max(as.numeric(c(lapa[2, ], norma[2, ], lapa[1, ]^pow)))
ymin = min(as.numeric(c(lapa[2, ], norma[2, ], lapa[1, ]^pow)))

X11()
plot(N, N^pow, log="xy", col="red", main = "LAPACK VS NO LAPACK", xlab = "Shuffles", ylab = "Seconds", ylim = c(ymin, ymax))
lines(N, N^pow, col = "red")
points(N, nola, col="blue")
lines(N, nola, col = "blue")
points(N, lapa, col = "green")
lines(N, lapa, col = "green")
legend("topright", legend = c("N^3", "NO LAPACK", "LAPACK"), pch = 15, col = c("red", "blue", "green"))

