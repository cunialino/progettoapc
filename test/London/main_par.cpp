#include"csv.h"
#include "point.h"
#include "matrix.h"
#include "gwr.h"
#include "lm.h"
#include<iostream>
#include <ctime>
#include <mpi.h>
#include <fstream>

using namespace lm;
int main(int argc, char * argv[])
{
  MPI_Init(&argc, &argv);
  int rank(0), s(0);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &s);
  std::size_t r , c;
  std::vector<double> fields;
  std::vector<point> pos;
  std::vector<double> xcoord, ycoord;
  if(rank == 0){
    csv csv_man;
    csv_man.parse_csv("london.csv", 2, 4);
    r = csv_man.get_rows();
    c = csv_man.get_cols();
    fields = csv_man.get_fields();
    pos = csv_man.get_pos();
    for(unsigned i =0; i < r; i++){
      xcoord.push_back(pos[i].get_ith_coord(0));
      ycoord.push_back(pos[i].get_ith_coord(1));
    }
  }
  MPI_Bcast(&r, 1, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);
  MPI_Bcast(&c, 1, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);
  xcoord.resize(r);
  ycoord.resize(r);
  MPI_Bcast(xcoord.data(), static_cast<int>(r), MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(ycoord.data(), static_cast<int>(r), MPI_DOUBLE, 0, MPI_COMM_WORLD);
  for(unsigned i =0; rank !=0 && i < r; i++){
    point tmp_p(2);
    tmp_p.set_ith_coord(0, xcoord[i]);
    tmp_p.set_ith_coord(1, ycoord[i]);
    pos.push_back(tmp_p);
  }
  fields.resize(r*c);
  MPI_Bcast(fields.data(), static_cast<int>(r*c), MPI_DOUBLE, 0, MPI_COMM_WORLD);
  std::vector<double> predic(r, 1);
  std::vector<double> tmp(fields.begin()+static_cast<long>(r), fields.end());
  predic.insert(predic.end(), tmp.begin(), tmp.end());
  std::vector<double> out(fields.begin(), fields.begin()+static_cast<long>(r));
  matrix X(predic, r, c);
  matrix y(out, r, 1);

  gwr lmg(X, y, pos, pos);
  double bd = lmg.bandwidth_selection();
  if(rank == 0)
    std::cout << "\nOptimal bandwidth: " << bd << "\n" << std::endl;
  lmg.fit();
  std::vector<matrix> betas = lmg.get_all_beta();
  std::vector<double> pvals = lmg.monte_carlo_test(100);
  if(rank == 0)
  for(auto i : pvals)
    std::cout << i << " ";
  std::cout << std::endl;
  MPI_Finalize();
  return 0;
}
