#include"csv.h"
#include "point.h"
#include "matrix.h"
#include "gwr.h"
#include "lm.h"
#include<iostream>
#include <fstream>

using namespace lm;
int main(int argc, char * argv[])
{
  std:size_t r , c;
  std::vector<double> fields;
  std::vector<point> pos;
  std::vector<double> xcoord, ycoord;
  csv csv_man;
  csv_man.parse_csv("london.csv", 2, 4);
  r = csv_man.get_rows();
  c = csv_man.get_cols();
  fields = csv_man.get_fields();
  pos = csv_man.get_pos();
  std::vector<double> predic(r, 1);
  std::vector<double> tmp(fields.begin()+static_cast<long>(r), fields.end());
  predic.insert(predic.end(), tmp.begin(), tmp.end());
  std::vector<double> out(fields.begin(), fields.begin()+static_cast<long>(r));
  matrix X(predic, r, c);
  matrix y(out, r, 1);
  gwr lmg(X, y, pos, pos);
  double bd = lmg.bandwidth_selection();
  std::cout << "Optimal bandwidth: " << bd << std::endl;
  lmg.fit();
  std::vector<matrix> betas = lmg.get_all_beta();
  std::vector<double> pval = lmg.monte_carlo_test(100);
  for(auto i : pval)
    std::cout << i << " ";
  std::cout << std::endl;
  return 0;
}
