#include "matrix.h"
#include "gwr.h"
#include "point.h"
#include <vector>
#include <iostream>
#include <set>
#include <ctime>

using namespace lm;

int main(){
  unsigned N=5;
  std::vector<point> pos;
  pos.reserve(N);
  matrix X(N, 2), y(N, 1);
  std::cout << "Loading matrix" << std::endl;
  for(unsigned i=0; i < N; i++){
    X(i, 0)=1;
    std::cin >> X(i, 1);
    std::cin >> y(i, 0);
  }
  for(unsigned i = 0; i < N; i++){
    point tmp(2);
    double val;
    std::cin >> val;
    tmp.set_ith_coord(0, val);
    std::cin >> val;
    tmp.set_ith_coord(1, val);
    pos.push_back(tmp);
  }
  std::cout << "matrix loaded" << std::endl;
  gwr ll(X, y, pos, pos);
  double bd = ll.bandwidth_selection();
  std::cout << "Optimal: " <<  bd << std::endl;
  time_t tstart, tend;
  tstart = time(0);
  ll.fit();
  tend = time(0);
  std::cout << "GWR fitted in: " << difftime(tend, tstart) << " s" << std::endl;
  matrix beta = ll.get_beta();
  std::cout << "Beta_hat: " << std::endl;
  std::cout << beta << std::endl;
  std::vector<double> pval = ll.monte_carlo_test();
  for(auto i : pval)
    std::cout << i << std::endl;
  return 0;
}
