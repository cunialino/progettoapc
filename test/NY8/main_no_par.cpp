#include"csv.h"
#include "point.h"
#include "matrix.h"
#include "gwr.h"
#include "lm.h"
#include<iostream>

using namespace lm;
int main()
{
  std::size_t r , c;
  std::vector<double> fields;
  std::vector<point> pos;
  csv csv_man;
  csv_man.parse_csv("NY8.csv", 2, 5);
  r = csv_man.get_rows();
  c = csv_man.get_cols();
  fields = csv_man.get_fields();
  pos = csv_man.get_pos();
  std::vector<double> predic(r, 1);
  std::vector<double> tmp(fields.begin()+2*static_cast<long>(r), fields.end());
  predic.insert(predic.end(), tmp.begin(), tmp.end());
  std::vector<double> out(fields.begin(), fields.begin()+static_cast<long>(r));
  matrix X(predic, r, c-1);
  matrix y(out, r, 1);
  std::vector<point> reg_pos;
  for(auto it = pos.cbegin(); it < pos.cend(); it+=50){
    reg_pos.push_back(*it);
  }
  gwr lmg(X, y, pos, reg_pos);
  double bd = lmg.bandwidth_selection();
  std::cout << "Band: " << bd << std::endl;
  std::size_t N=10;
  std::vector<double> times(4);
  std::vector<std::size_t> shuffles(4);
  for(std::size_t i = 0; i < 4; i++, N*=10){
    std::cout << "Montecarlo with: " << N << '\n';
    clock_t c_start, c_diff;
    c_start = clock();
    std::vector<double> pval = lmg.monte_carlo_test(N);
    c_diff = clock()-c_start;
    double c_sec = static_cast<double>(c_diff)/CLOCKS_PER_SEC;
    times[i] = c_sec;
    shuffles[i] = N;
  }
  std::vector<double> oo;
  oo.insert(oo.end(), shuffles.begin(), shuffles.end());
  oo.insert(oo.end(), times.begin(), times.end());
  csv_man.write_csv("output_lapa.csv", oo, shuffles.size());
  return 0;
}
