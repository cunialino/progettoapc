#include"csv.h"
#include "point.h"
#include "matrix.h"
#include "gwr.h"
#include "lm.h"
#include <ctime>
#include <mpi.h>
#include<iostream>

using namespace lm;
int main(int argc, char * argv[])
{
  MPI_Init(&argc, &argv);
  int rank(0), s(0);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &s);
  std::size_t r , c;
  std::vector<double> fields;
  std::vector<point> pos;
  std::vector<double> xcoord, ycoord;
  csv csv_man;
  if(rank == 0){
    csv_man.parse_csv("NY8.csv", 2, 5);
    r = csv_man.get_rows();
    c = csv_man.get_cols();
    fields = csv_man.get_fields();
    pos = csv_man.get_pos();
    for(unsigned i =0; i < r; i++){
      xcoord.push_back(pos[i].get_ith_coord(0));
      ycoord.push_back(pos[i].get_ith_coord(1));
    }
  }
  MPI_Bcast(&r, 1, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);
  MPI_Bcast(&c, 1, MPI_UNSIGNED_LONG, 0, MPI_COMM_WORLD);
  xcoord.resize(r);
  ycoord.resize(r);
  MPI_Bcast(xcoord.data(), static_cast<int>(r), MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Bcast(ycoord.data(), static_cast<int>(r), MPI_DOUBLE, 0, MPI_COMM_WORLD);
  for(unsigned i =0; rank !=0 && i < r; i++){
    point tmp_p(2);
    tmp_p.set_ith_coord(0, xcoord[i]);
    tmp_p.set_ith_coord(1, ycoord[i]);
    pos.push_back(tmp_p);
  }
  fields.resize(r*c);
  MPI_Bcast(fields.data(), static_cast<int>(r*c), MPI_DOUBLE, 0, MPI_COMM_WORLD);
  std::vector<double> predic(r, 1);
  std::vector<double> tmp(fields.begin()+2*static_cast<long>(r), fields.end());
  predic.insert(predic.end(), tmp.begin(), tmp.end());
  std::vector<double> out(fields.begin(), fields.begin()+static_cast<long>(r));
  matrix X(predic, r, c-1);
  matrix y(out, r, 1);

  std::vector<point> reg_pos;
  for(auto it = pos.cbegin(); it < pos.cend(); it+=50){
    reg_pos.push_back(*it);
  }
  gwr lmg(X, y, pos, reg_pos);
  lmg.bandwidth_selection();
  std::size_t N = 10;
  std::vector<double> times(4);
  std::vector<std::size_t> shuffles(4);
  for(unsigned i = 0; i < 4; i++, N*=10){
    clock_t c_s, c_e;
    if(rank == 0)
      std::cout << "Monte carlo with N=" << N << std::endl;
    c_s = clock();
    std::vector<double> pval = lmg.monte_carlo_test(N);
    c_e = clock()-c_s;
    double time = static_cast<double>(c_e)/CLOCKS_PER_SEC;
    if(rank == 0){
      times[i] = time;
      shuffles[i] = N;
    }
  }
  if(rank == 0){
    std::vector<double> oo;
    oo.insert(oo.end(), shuffles.begin(), shuffles.end());
    oo.insert(oo.end(), times.begin(), times.end());
    csv_man.write_csv("output_para.csv", oo, shuffles.size());
  }
  MPI_Finalize();
  return 0;
}
