#include "NumericalAlgorithms.h"
#include <functional>
#include <iostream>

double func(double x){
  return (x-1)*(x-1);
}

int main(){
  std::function<double (double)> f = func;
  double min = my_numerics::gradient_descent_momentum(f, 10);
  std::cout << "Minimum is at: " << min << std::endl;
  std::cout << "With value: " << f(min) << std::endl;
  return 0;
}
