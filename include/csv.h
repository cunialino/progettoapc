#ifndef CSV_H
#define CSV_H

#include <iostream>
#include <vector>
#include <string>
#include "point.h"

class csv{
  private:
    std::size_t rows=0;
    std::size_t cols=0;
    std::vector<double> fields;
    std::vector<point> pos;
    bool has_header = false;
    std::vector<std::string> col_names{};
  public:
    csv() {}
    std::vector<double> get_fields(void) const {return fields;}
    std::vector<point> get_pos(void) const {return pos;}
    std::size_t get_rows(void) const {return rows;}
    std::size_t get_cols(void) const {return cols;}
    void parse_csv(const std::string & , std::size_t , std::size_t , bool header = true, const char & sep = ',');
    void write_csv(const std::string & , std::vector<double> const & , std::size_t , const char & sep = ',');
};


#endif /* end of include guard: READ_CSV_H_ */
