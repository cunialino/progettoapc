#ifndef HAVE_MATRIX_H
#define HAVE_MATRIX_H

#include <vector>
#include <iostream>

class
matrix
{

private :

  std::vector<double> data;
  std::size_t rows=0;
  std::size_t cols=0;

  inline std::size_t
  sub2ind (const std::size_t ir,
           const std::size_t jc) const
  { return (ir + jc*rows); }

  double &
  index (std::size_t irow, std::size_t jcol)
  { return data[sub2ind (irow, jcol)]; }

  const double &
  const_index (std::size_t irow, std::size_t jcol) const
  { return data[sub2ind (irow, jcol)]; }

  bool factorized;

public :

  matrix()=default;
  matrix(std::vector<double> v, std::size_t r, std::size_t c):data(v), rows(r), cols(c) {}
  matrix (std::size_t size): rows (size), cols (size) { data.resize (rows * cols, 0.0); }
  matrix (std::size_t rows_, std::size_t cols_): rows (rows_), cols (cols_) { data.resize (rows * cols, 0.0); }

  matrix (matrix const &) = default;

  std::size_t get_rows () const { return rows; }
  std::size_t get_cols () const { return cols; }

  double & operator() (std::size_t irow, std::size_t jcol) { return index (irow, jcol); }

  const double & operator()  (std::size_t irow, std::size_t jcol) const { return const_index (irow, jcol); }

  const double * get_data () const { return &(data[0]); }

  double * get_data () { return &(data[0]); }

  void swap_data(std::vector<double> vals, unsigned rows, unsigned cols);

  /// transposed matrix : B = A'
  matrix transpose () const;

  matrix solve (matrix &rhs);

  //returns X s.t. A*X=I
  matrix inv_chol(void);

  void factorize ();

  matrix& operator= (const matrix& B);
  friend std::ostream & operator<<(std::ostream & os, const matrix & A);
  matrix operator-(const matrix& B);
  matrix& operator*=(double d);

};

/// matrix x matrix product : C = A * B
matrix operator* (const matrix& A, const matrix& B);

#endif
