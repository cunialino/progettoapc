#ifndef POINT_H
#define POINT_H

#include <vector>
#include <iostream>

class point{
  protected:
    std::vector<double> coords;
    std::size_t dim=0;
    bool longlat = false;

  public:
    point() {}
    point(std::size_t N): dim(N) {coords.resize(dim);}

    point(std::vector<double> const & v): coords(v), dim(v.size()) {}

    std::size_t get_dim(void) const {return dim;}

    std::vector<double> get_coords(void) const {return coords;}

    double get_ith_coord(std::size_t i) const {return coords[i];}

    void set_ith_coord(std::size_t i, double val) {coords[i] = val;}

    void set_coord(std::vector<double> v) {coords = v;}

    double distance(point const & p2) const;
    bool operator<(point const &p2);
};

#endif
