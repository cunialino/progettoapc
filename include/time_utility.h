#ifndef TIME_UTILITY_H_
#define TIME_UTILITY_H_



#ifndef msize
#define msize 1000
#endif

static clock_t c_start, c_diff;
static double c_msec;
#define tic() c_start = clock ();
#define toc(x) c_diff = clock () - c_start;                   \
  c_msec = (double)c_diff / (double)CLOCKS_PER_SEC;    \
  std::cout << x << c_msec << std::endl;

#endif /* end of include guard: TIME_UTILITY_H_MOYJFVQZ */
