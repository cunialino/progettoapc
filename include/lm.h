#ifndef LINEAR_MODEL
#define LINEAR_MODEL

#include <vector>
#include "matrix.h"
#include "min_square.h"

namespace lm
{
  class linear_model:public min_square
  {
  private:
    std::vector<double> p_val;

    void t_tests(matrix & min_sqr);

  public:
    linear_model(matrix const & XX, matrix const & yy): min_square(XX, yy) { p_val.resize(X.get_cols()); }
    //method that executes classic linear regression
    //inputs: matrix of response variable, matrix of the independent variables
    void fit();
    std::vector<double> get_pvalues(void) const {return p_val;}


  };

}

#endif
