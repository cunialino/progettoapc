#ifndef MIN_SQUARE_H_
#define MIN_SQUARE_H_

#include "matrix.h"

namespace lm
{
  class min_square{
  protected:
    matrix X;
    matrix y;
    matrix beta_hat;
    matrix err;
    matrix y_hat;
    double sigma_hat=-1;

    void fit_minsqr(matrix & min_sqr, matrix & rhs);
    min_square(matrix const & XX, matrix const & yy): X(XX), y(yy), beta_hat(X.get_cols(), 1), err(yy.get_rows(), 1), y_hat(yy.get_rows(), 1) {}

  public:
    matrix get_beta(void) const{return beta_hat;}
    matrix get_y_hat(void) const { return y_hat; }
    double get_sigma_hat(void) const {return sigma_hat;}


  };

}


#endif /* end of include guard: MIN_SQUARE_H_ */
