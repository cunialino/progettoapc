#ifndef GWR_H_
#define GWR_H_

#include <vector>
#include <functional>
#include <iostream>
#include "matrix.h"
#include "point.h"
#include "min_square.h"

namespace lm
{
  matrix gaussian_ker(point, std::vector<point> const & pos, double h, std::size_t num_i = 0, bool no_i = false);
  matrix bisquare_ker(point, std::vector<point> const & pos, double h, std::size_t num_i = 0, bool no_i = false);
  class gwr: public min_square
  {
  private:
    //Positions of datas
    std::vector<point> datas_pos;
    //Location around which you want to use gwr
    typedef std::function<matrix (point const &, std::vector<point> const &, double, std::size_t, bool)> ker_type;
    std::vector<point> reg_pos;
    //Needs this to store different betas according to position
    std::vector<matrix> beta_hats;
    std::vector<matrix> y_hats;
    std::vector<matrix> errs;

    ker_type ker;

    bool bandwidth_optimized = false;
    double bandwidth = 1;
    double opt_bandwidth = 1;
    std::pair<double, double> bandwidth_range = std::make_pair(1, 1);

    double cvss(double);
    double compute_v_stat(std::size_t);
    void max_bandwidth_initialization();

  public:
    gwr(matrix const & XX, matrix const & yy, std::vector<point> const & d_p,
      std::vector<point> const & r_p, ker_type k=gaussian_ker): min_square(XX, yy),
      datas_pos(d_p), reg_pos(r_p), ker(k)
      {
        beta_hats.reserve(reg_pos.size());
        y_hats.reserve(reg_pos.size());
        errs.reserve(reg_pos.size());
        max_bandwidth_initialization();
      }

    void fit(bool no_i = false);

    std::vector<double> monte_carlo_test(const std::size_t NUMSHFFLE = 100);
    double bandwidth_selection(void);
    std::vector<matrix> get_all_beta(void) const {return beta_hats;}


  };

}

#endif
