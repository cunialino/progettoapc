#ifndef NUMERICALALGORITHMS_H_
#define NUMERICALALGORITHMS_H_

#include <functional>

namespace my_numerics{
  double gradient_descent(std::function<double ( double )> f, double h0, double step = 0.1, double toll=0.01);
  double gradient_descent_momentum(std::function<double ( double )> f, double h0, double step = 0.01, double toll=0.01);
  double golden_selection(std::function<double (double)>d, double a, double b, double toll = 0.001);
}


#endif /* end of include guard: NUMERICALALGORITHMS_H_ */



